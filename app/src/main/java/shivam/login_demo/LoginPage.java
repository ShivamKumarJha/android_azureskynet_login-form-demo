package shivam.login_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginPage extends AppCompatActivity {

    EditText e1,e2;
    Button b1,b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        b1 = (Button) findViewById(R.id.but1);
        b2 = (Button) findViewById(R.id.but2);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(e1.getText().toString().equals("shivam") && e2.getText().toString().equals("skj"))
                {
                    Toast.makeText(LoginPage.this, "Valid user", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(LoginPage.this, Homepage.class);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(LoginPage.this, "Invalid user", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(LoginPage.this, Error.class);
                    startActivity(i);
                }

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginPage.this, Register.class);
                startActivity(i);
            }
        });
    }
}
